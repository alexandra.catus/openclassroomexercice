import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input() PostTitle: string;
  @Input() PostContent: string;
  @Input() PostLoveIts: number;
  @Input() PostCreated_at: Date;
  constructor() { }

  ngOnInit() {
  }
  loveIts( ajout ) {
      this.PostLoveIts += ajout;

}
}
