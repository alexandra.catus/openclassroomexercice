import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  posts = [
    {
      title: 'Mon premier post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie sed mi et massa nunc.',
      loveIts: 0,
      created_at: Date.now()
    }, {
      title: 'Mon deuxième post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie sed mi et massa nunc.',
      loveIts: 0,
      created_at: Date.now()
    }, {
      title: 'Encore un post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie sed mi et massa nunc.',
      loveIts: 0,
      created_at: Date.now()
    }

  ];

}

